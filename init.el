
;; ======================================== ;;
;;       Package Manager Initializer
;; ======================================== ;;
;;; MELPA PACKAGE MANAGER
(require 'package)
(add-to-list 'package-archives
			 '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; ======================================== ;;
;;            Global Theming
;; ======================================== ;;
;;; Global Theme
(use-package doom-themes
	     :ensure t
	     :config
	     ;; Global settings (defaults)
	     (setq doom-themes-enable-bold t ; if nil, disable bold
		   doom-themes-enable-italic t)  ; if nil, disable italics
	     (load-theme 'doom-moonlight t)

	     ;; Corrects (and improves) org-mode's native fontification
	     (doom-themes-org-config))

;; ======================================== ;;
;;           General Packages
;; ======================================== ;;
(use-package vterm
  :ensure t)

(use-package flycheck
  :ensure t
  :config
  (add-hook 'sh-mode-hook 'flycheck-mode))

(use-package gnuplot-mode
  :ensure t)

;; ======================================== ;;
;;              Line Numbering
;; ======================================== ;;
(require 'display-line-numbers)

(defcustom display-line-numbers-exempt-modes
  '(vterm-mode eshell-mode shell-mode term-mode ansi-term-mode)
  "Major modes on which to disable line numbers."
  :group 'display-line-numbers
  :type 'list
  :version "green")

(defun display-line-numbers--turn-on ()
  "Turn on line numbers except for certain major modes.
   Exempt major modes are defined in `display-line-numbers-exempt-modes'."
  (unless (or (minibufferp)
              (member major-mode display-line-numbers-exempt-modes))
    (display-line-numbers-mode)))

(global-display-line-numbers-mode)

;; ======================================== ;;
;;            Python IDE Setup
;; ======================================== ;;
;; ---------------------------------------- ;;
;;            Python Packages
;; ---------------------------------------- ;;
;; Required Package List
(defvar python-packages
  '(elpy     ;; Emacs Lisp Python Environment
	magit    ;; Git integration
    )
  )

;; Install packages if not installed
(mapc #'(lambda (package)
	  (unless (package-installed-p package)
	    (package-install package)))
      python-packages)

;; Enable elpy and flycheck into it
(elpy-enable)
(when (require 'flycheck nil t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))

;; ---------------------------------------- ;;
;;            Python IDE Edits
;; ---------------------------------------- ;;
;; Make C-c C-c behave like C-u C-c C-c in Python mode
(require 'python)
(define-key python-mode-map (kbd "C-c C-c")
  (lambda () (interactive) (elpy-shell-send-buffer t)))

;; ======================================== ;;
;;            LaTeX IDE Setup
;; ======================================== ;;
;; Better pdf viewer
(use-package pdf-tools
  :ensure t
  :config
  ;; Exectuable locations on Mac -- Need to update when necessary
  (if (eq system-type 'darwin)
	  (setenv "PKG_CONFIG_PATH" (concat "/usr/local/opt/zlib/lib/pkgconfig:"
                                        "/usr/local/lib/pkgconfig:"
                                        "/usr/X11/lib/pkgconfig:"
                                        "/usr/local/Cellar/poppler/22.08.0/lib/pkgconfig:"
                                        "/opt/x11/share/pkgconfig")
	  )
  )
  (pdf-tools-install)
  (custom-set-variables
   '(pdf-tools-handle-upgrades t)))
(add-hook 'pdf-view-mode-hook (lambda() (linum-mode -1)))
(add-hook 'pdf-tools-enabled-hook 'pdf-view-midnight-minor-mode)

;; Latex configuration
(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)

(add-hook 'LaTeX-mode-hook 'visual-line-mode)
(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)

(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(setq reftex-plug-into-AUCTeX t)

(setq TeX-PDF-mode t)
(add-hook 'doc-view-mode-hook 'auto-revert-mode)

;; ======================================== ;;
;;            Bash IDE Setup
;; ======================================== ;;
;; Shell configuration
(setq sh-basic-offset 2)
(setq sh-indentation 2)

;; ======================================== ;;
;;            Haskell IDE Setup
;; ======================================== ;;

(use-package haskell-mode
  :ensure t)
(add-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode)

;; If you use hslint, this makes indentation proper
(add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)

;; ======================================== ;;
;;               C IDE Setup
;; ======================================== ;;
;; Indent style
(setq c-default-style "k&r"  ; Basic offset style
      c-basic-offset 4)      ; Default indentation level

;; Syntax highlighting
(add-hook 'c-mode-common-hook
          (lambda () (setq flycheck-gcc-language-standard "c++11")))

;; ---------------------------------------- ;;
;;              LSP Setup
;; ---------------------------------------- ;;
(setq package-selected-packages '(lsp-mode yasnippet lsp-treemacs helm-lsp
                                           projectile hydra company avy
                                           which-key helm-xref dap-mode))
(when (cl-find-if-not #'package-installed-p package-selected-packages)
  (package-refresh-contents)
  (mapc #'package-install package-selected-packages))

;; Sample `helm' configuration
(helm-mode)
(require 'helm-xref)
(define-key global-map [remap find-file] #'helm-find-files)
(define-key global-map [remap execute-extended-command] #'helm-M-x)
(define-key global-map [remap switch-to-buffer] #'helm-mini)

(which-key-mode)
(add-hook 'c-mode-hook 'lsp)
(add-hook 'c++-mode-hook 'lsp)

(setq gc-cons-threshold (* 100 1024 1024)
      read-process-output-max (* 1024 1024)
      treemacs-space-between-root-nodes nil
      company-idle-delay 0.0
      company-minimum-prefix-length 1
      lsp-idle-delay 0.1)

(with-eval-after-load 'lsp-mode
  (add-hook 'lsp-mode-hook #'lsp-enable-which-key-integration)
  (require 'dap-cpptools)
  (yas-global-mode))

;; ======================================== ;;
;;         Org-Mode Customization
;; ======================================== ;;
;; Org-Mode Settings
(global-set-key (kbd "C-c l") #'org-store-link)
(global-set-key (kbd "C-c a") #'org-agenda)
(global-set-key (kbd "C-c c") #'org-capture)

;; ======================================== ;;
;;           Eshell customization
;; ======================================== ;;
;; Custom (Bash-like) eshell prompt
(setq eshell-prompt-regexp "^[^#$\n]*[#$] "
	  eshell-prompt-function
	  (lambda nil
		(concat
		 "[" (user-login-name) "@" (system-name) " "
		 (if (string= (eshell/pwd) (getenv "HOME"))
			 "~" (eshell/basename (eshell/pwd)))
		 "]"
		 (if (= (user-uid) 0) "# " "$ "))))

;; ======================================== ;;
;;         General Look & Feel
;; ======================================== ;;
;;; Disable menubar and toolbar
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(setq inhibit-startup-message t)     ; Hide the startup message

;; Global tab behavior
(setq-default indent-tabs-mode nil)  ; Spaces for tabs
(setq-default tab-width 4)

;; ======================================== ;;
;;         Behavior on MacOS
;; ======================================== ;;
;; Mac Specific Settings
(if (eq system-type 'darwin)
  (progn
    (setq mac-command-modifier 'meta)
    (add-to-list 'default-frame-alist '(ns-transparent-titlebar . t))
    (add-to-list 'default-frame-alist '(ns-appearance . dark))
    (set-terminal-coding-system 'utf-8)
    (set-keyboard-coding-system 'utf-8)
    (prefer-coding-system 'utf-8)
	(setenv "WORKON_HOME" "/opt/anaconda3/envs")
    (setq python-shell-interpreter "/opt/anaconda3/bin/python3")
	(setq python-shell-completion-native-enable nil)
	(setq pdf-view-use-scaling t) ; Cures fuzzy pdfs
    (set-frame-font "DejaVu Sans Mono for Powerline 20" nil t))
  (setenv "WORKON_HOME" "/home/dylan/miniconda3/envs")
  (setq default-frame-alist '((font . "Monospace-16"))))

;; ======================================== ;;
;;          Behavior on Linux
;; ======================================== ;;
(if (eq system-type 'gnu/linux)
    (progn
      (pdf-tools-install)))

;; ======================================== ;;
;;      Customizing Windowing Commands
;; ======================================== ;;
;; Define function for moving "back" a window
;;     Allows for more than one window change with
;;     optional "n" argument. Defaults to one back
;;     otherwise.
(defun other-window-backward (&optional n)
  "Select the nth previous window."
  (interactive "P")
  (other-window (- (prefix-numeric-value n))))

(global-set-key "\C-x\C-p" 'other-window-backward) ; Reverse change window
(global-set-key "\C-x\C-n" 'other-window)          ; Change window to C-x-C-n

;; ======================================== ;;
;;        Customizing Scrolling
;; ======================================== ;;
;; Change scrolling names to more intuitive versions
(defalias 'scroll-ahead 'scroll-up)
(defalias 'scroll-behind 'scroll-down)

;; Define functions for scrolling by one
(defun scroll-n-lines-ahead (&optional n)
  "Scroll ahead n lines (1 by default)."
  (interactive "P")
  (scroll-ahead (prefix-numeric-value n)))
(defun scroll-n-lines-behind (&optional n)
  "Scroll behind n lines (1 by default)."
  (interactive "P")
  (scroll-behind (prefix-numeric-value n)))

;; Set new keybindings for line scrolling
(global-set-key "\C-q" 'scroll-n-lines-behind)
(global-set-key "\C-z" 'scroll-n-lines-ahead)

;; Reset command for \C-q bound to "quoted-insert"
(global-set-key "\C-x\C-q" 'quoted-insert)

;; ======================================== ;;
;;        Customizing Cursor Motion
;; ======================================== ;;
;; ---------------------------------------- ;;
;;               Point to Top
;; ---------------------------------------- ;;
;; Define function for postioning cursor to top of window
(defun point-to-top ()
  "Put point on top line of window."
  (interactive)
  (move-to-window-line 0))

;; Set key for moving to top of window
(global-set-key "\M-," 'point-to-top)
;; Reset old keybinding for 'tags-loop-continue
(global-set-key "\C-x," 'tags-loop-continue)

;; ---------------------------------------- ;;
;;             Point to Bottom
;; ---------------------------------------- ;;
;; Define function for positioning cursor to bottom of window
(defun point-to-bottom ()
  "Put point at beginning of last visible line."
  (interactive)
  (move-to-window-line -1))

;; Set key for moving to bottom of window
(global-set-key "\M-." 'point-to-bottom)

;; ---------------------------------------- ;;
;;              Line to Top
;; ---------------------------------------- ;;
;; Moves the line of the cursor to the top of the window
;; Eliminates shortcut for shell-command
;;     Define functionality for this:
(defun line-to-top()
  "Move current line to top of window."
  (interactive)
  (recenter 0))

;; Set keybinding for making top line
(global-set-key "\M-!" 'line-to-top)

;; ======================================== ;;
;;       Behavior Opening SymLinks
;; ======================================== ;;
;; When SymLink is opened, it begins as read-only.
;;    The options on what to do afterwards are then
;;    decided.

;; Function to automatically open read-only
(defun read-only-if-symlink ()
  (if (file-symlink-p buffer-file-name)
	  (progn
		(setq buffer-read-only t)
		(message "File is a symlink"))))
;; Function for visiting symlink file to edit
(defun visit-target-instead ()
  "Replace this buffer with a buffer visiting the link target."
  (interactive)
  (if buffer-file-name
	  (let ((target (file-symlink-p buffer-file-name)))
		(if target
			(find-alternate-file target)
		  (error "Not visiting a symlink")))
	(error "Not visiting a file.")))
;; Function to replace symlink with actual copy of linked file
(defun clobber-symlink ()
  "Replace symlink with a copy of the file."
  (interactive)
  (if buffer-file-name
	  (let ((target (file-symlink-p buffer-file-name)))
		(if target
			(if (yes-or-no-p (format "Replace %s with %s? "
									 buffer-file-name
									 target))
				(progn
				  (delete-file buffer-file-name)
				  (write-file buffer-file-name)))
		  (error "Not visiting a symlink")))
	(error "Not visiting a file")))

;; Add function to hook when opening file
(add-hook 'find-file-hooks 'read-only-if-symlink)

;; Add keybindings for editing symlink or replacing with copy
(global-set-key "\C-xt" 'visit-target-instead)
(global-set-key "\C-xl" 'clobber-symlink)

;; ======================================== ;;
;;        Automated (Custom) Setup
;; ======================================== ;;
;; Custom variables
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(ggtags markdown-mode pdf-tools auctex elpy use-package doom-themes))
 '(pdf-tools-handle-upgrades t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
